{ pkgs ? import <nixpkgs> {} }:

with pkgs;

mkShell {
  buildInputs = [
    plantuml
    graphviz
    jdk
  ];
}
